# SourceGit

开源的Git客户端，仅用于Windows 10。单文件，无需安装，< 500KB。

**由于有人希望参与进来，为方便之后合作，仓库迁移到[SourceGit/SourceGit](https://gitee.com/sourcegit/SourceGit). 本仓库不再更新！！！**

* DarkTheme

![Preview_Dark](./Preview_Dark.png)

* LightTheme

![Preview_Light](./Preview_Light.png)

